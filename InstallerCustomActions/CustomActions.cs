using Microsoft.Deployment.WindowsInstaller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Web.Administration;

namespace InstallerCustomActions
{
    public class CustomActions
    {
        [CustomAction]
        public static ActionResult AddBindings(Session session)
        {
            session.Log("Begin EnableProtocols");

            var siteName = session.CustomActionData["SITE"];
            var portsString = session.CustomActionData["PORTS"];
            var protocolsString = session.CustomActionData["PROTOCOLS"];

            if (string.IsNullOrEmpty(siteName))
            {
                session.Log("Property [SITE] missing");
                return ActionResult.NotExecuted;
            }

            if (string.IsNullOrEmpty(portsString))
            {
                session.Log("Property [PORTS] missing");
                return ActionResult.NotExecuted;
            }

            if (string.IsNullOrEmpty(protocolsString))
            {
                session.Log("Property [PROTOCOLS] missing");
                return ActionResult.NotExecuted;
            }

            var ports = portsString.Split(',');
            foreach (var port in ports)
            {
                session.Log("port is {0}", port);
            }
            var protocols = protocolsString.Split(',');

            if (protocols.Length != ports.Length)
            {
                session.Log("[PORTS] and [PROTOCOLS] are required to be of equal length");
                return ActionResult.NotExecuted;
            }

            var manager = new ServerManager();
            var site = manager.Sites.FirstOrDefault(s => s.Name == siteName);
            if (site == null)
            {
                session.Log("Site [SITE] does not exist");
                return ActionResult.NotExecuted;
            }

            var application = site.Applications.FirstOrDefault(app => app.Path == "/");
            if (application == null)
            {
                session.Log("Site Application for path '/' does not exist");
                return ActionResult.NotExecuted;
            }

            session.Log("found wcf {0}", site.Name);

            try
            {
                site.Bindings.Clear();
                session.Log("clear bindings");

                // assuming ports and protocols have equal length
                for (int i = 0; i < ports.Length; i++)
                {
                    site.Bindings.Add(ports[i], protocols[i]);
                    session.Log("add binding {0} {1}", ports[i], protocols[i]);
                }
                application.EnabledProtocols = protocolsString;
                manager.CommitChanges();
                session.Log("changes applied");
                return ActionResult.Success;
            }
            catch (Exception e)
            {
                session.Log("Error setting enabled protocols: {0}", e.ToString());
                return ActionResult.Failure;
            }
        }
    }
}
